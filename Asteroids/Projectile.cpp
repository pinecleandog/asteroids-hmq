#include "Projectile.h"
#include <SFML\Graphics.hpp>
#include "Application.h"
#include "MathUtil.h"

Projectile::Projectile()
{
	name = "Projectile";
	bounds = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(4, 4));
}

void Projectile::spawn(sf::Vector2f pos, float angle, float duration)
{
	direction = MathUtil::angleToDir(angle);
	setPosition(pos);
	timeLeft = duration;
	setEnabled(true);
}

void Projectile::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
{
	if (!isEnabled()) return;

	// Draw a simple 2 point line
	sf::VertexArray line(sf::LinesStrip, 2);
	line[0] = sf::Vertex(getPosition());
	line[1] = sf::Vertex(getPosition() + (direction * length));

	rt.draw(line, rs);
}

void Projectile::update(float deltaTime)
{
	if (!isEnabled()) return;

	sf::Vector2f newPos = getPosition() + direction * speed * deltaTime;
	newPos = MathUtil::screenWrapPosition(newPos);
	setPosition(newPos);

	timeLeft -= deltaTime;
	if (timeLeft <= 0)
	{
		setEnabled(false);
	}
}

void Projectile::handleCollision(Entity& other)
{
	// TODO: find way to clean up these cases if possible
	if (name == "PlayerProjectile" && other.name == "Ship") return;
	if (other.name == "PlyProjectile" || other.name == "EnemyProjectile") return;
	if (name == "EnemyProjectile" && other.name == "UFO") return;

	setEnabled(false);
}

sf::FloatRect Projectile::getBounds() const
{
	return bounds;
}

bool Projectile::collisionEnabled() const
{
	return isEnabled();
}
