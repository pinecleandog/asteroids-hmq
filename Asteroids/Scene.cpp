#include "Scene.h"
#include <SFML\Graphics\RenderWindow.hpp>
#include "ICollidable.h"
#include "Collision.h"
#include <vector>

void Scene::addEntity(std::shared_ptr<Entity>& entity)
{
	newEntities.push_back(std::move(entity));
}

void Scene::draw(sf::RenderWindow& window) const
{
	for (auto it = entities.begin(); it != entities.end(); it++)
	{
		if (*it == nullptr) continue;

		window.draw(**it);
	}
}

void Scene::update(float deltaTime)
{
	// Update behaviours
	auto it = entities.begin();

	while (it != entities.end())
	{
		if (*it == nullptr)
		{
			// Clean up any entity that is no longer a reference to anything
			it = entities.erase(it);
		}
		else
		{
			(*it)->update(deltaTime);
			++it;
		}
	}

	// Overlap checks
	std::vector<Collision> collisions;
	int colCount = 0;

	// Go through each object and every other object.
	// TODO: look into implementing some kind of spatial partioning
	for (int i = 0; i < entities.size(); i++)
	{
		for (int j = 0; j < entities.size(); j++)
		{
			// Both entities are the same entity
			if (i == j)continue;

			auto col1 = std::dynamic_pointer_cast<ICollidable>(entities[i]);
			auto col2 = std::dynamic_pointer_cast<ICollidable>(entities[j]);

			if (col1 == nullptr || col2 == nullptr) continue;
			if (!col1->collisionEnabled() || !col2->collisionEnabled()) continue;

			auto bounds1 = convertBoundsToGlobal(col1->getBounds(), *entities[i]);
			auto bounds2 = convertBoundsToGlobal(col2->getBounds(), *entities[j]);

			// If it intersects add collision data to vector to process later
			if (bounds1.intersects(bounds2))
			{
				Collision collision(colCount,
					entities[i].get(),
					entities[j].get(),
					col1.get(),
					col2.get());
				collisions.push_back(collision);
				colCount++;
			}
		}
	}

	// handle collisions after overlap checks to prevent colliders from being disabled
	// as a result from a collision event
	for (auto collison : collisions)
	{
		if (collison.col1 == nullptr || collison.ent2 == nullptr) continue;

		collison.col1->handleCollision(*collison.ent2);
	}

	// newly created entities are moved into the main entity array at the end
	// of each update cycle, because they may have been created during an iteration,
	// invalidating the iterator
	entities.insert(
		entities.end(),
		std::make_move_iterator(newEntities.begin()),
		std::make_move_iterator(newEntities.end()));

	newEntities.clear();
}

sf::FloatRect Scene::convertBoundsToGlobal(sf::FloatRect bounds, const Entity& ent)
{
	bounds.top = ent.getPosition().y;
	bounds.left = ent.getPosition().x - bounds.width / 2;
	return bounds;
}

Scene::~Scene()
{
	// force all memory for entities is freed when a scene is unloaded
	for(auto it = entities.begin(); it != entities.end(); it++)
	{
		(*it).reset();
	}

	entities.clear();
}