#pragma once

// Gameplay controllers drive the main logic of the game and change the
// behaviour based on the game's state. Usually responsible spawning and
// managing entites.
class IGameplayController
{
public:
	// Called once the game has started
	virtual void begin() =0;
	// Called when the game has ended
	virtual void end() =0;
	// Called when the level is cleared, and the next level starts
	virtual void advanceLevel(int level) =0;
	// Called every frame
	virtual void update(float deltaTime) =0;
};