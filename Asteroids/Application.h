#pragma once

#include <SFML/Graphics.hpp>
#include "Scene.h"
#include "GameManager.h"

// Main application class created from the program's entry point
class Application
{
public:
	Application();
	~Application();

	// Is the application currently running
	bool isRunning()const;
	// Quit the application
	void exit();
	// Called from the main loop - updates the application state
	void update();
	// Get the width of the application's window
	static unsigned int getScrnW();
	// Get the height of the application's window
	static unsigned int getScrnH();
	// Get a pointer to the application's window, if it has been created
	static const sf::RenderWindow* getWindow();
private:
	static Application* instance;
	sf::RenderWindow* window;
	bool running;
	sf::Clock frameTimer;
};