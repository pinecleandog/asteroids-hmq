#include "Ship.h"
#include "Application.h"
#include "MathUtil.h"

using namespace Entities;

Ship::Ship()
{
	name = "Ship";
	buildShape();
	reset();
	setEnabled(true);
}

void Ship::buildShape()
{
	shipShape.setPointCount(3);
	shipShape.setPoint(0, sf::Vector2f(15.f, 0.f));
	shipShape.setPoint(1, sf::Vector2f(-15.f, 10.f));
	shipShape.setPoint(2, sf::Vector2f(-15.f, -10.f));

	shipShape.setFillColor(sf::Color::Black);
	shipShape.setOutlineColor(sf::Color::White);
	shipShape.setOutlineThickness(1.f);
	
	shipShape.setPosition(0.f, 0.f);
	shipShape.setRotation(0.f);
}

void Ship::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
{
	if (!isEnabled()) return;
	rs.transform *= getTransform();
	rt.draw(shipShape, rs);
}


void Ship::update(float deltaTime)
{
	if (!isEnabled()) return;
	applyMovement(deltaTime);
	applyRotation(deltaTime);
	projectileSpawner.update(deltaTime);
}

void Ship::setLookPos(sf::Vector2f pos)
{
	lookPos = pos;
}

void Ship::setMovement(sf::Vector2i input)
{
	moveInput = input;
}

void Ship::applyMovement(float deltaTime)
{
	// update velocities based on linear acceleration

	if (moveInput.x != 0)
	{
		velocity.x += (float)moveInput.x * acceleration * deltaTime;
	}
	else if(velocity.x != 0) // no input - deaccelerating
	{
		velocity.x -= MathUtil::sign(velocity.x) * acceleration * deltaTime;
		if (std::abs(velocity.x) <= 0.01f)
			velocity.x = 0;
	}

	if (moveInput.y != 0)
	{
		velocity.y += (float)moveInput.y * acceleration * deltaTime;
	}
	else if(velocity.y != 0) // no input - deaccelerating
	{
		velocity.y -= MathUtil::sign(velocity.y) * acceleration * deltaTime;
		if (std::abs(velocity.y) <= 0.01f)
			velocity.y = 0;
	}

	velocity.x = MathUtil::clamp(velocity.x, -maxSpeed, maxSpeed);
	velocity.y = MathUtil::clamp(velocity.y, -maxSpeed, maxSpeed);

	sf::Vector2f newPos = getPosition() + velocity * deltaTime;
	newPos = MathUtil::screenWrapPosition(newPos);
	setPosition(newPos);
}

void Ship::applyRotation(float deltaTime)
{
	sf::Vector2f lookDir = getPosition() - (sf::Vector2f)lookPos;
	lookDir = MathUtil::normalize(lookDir);

	float rotation = MathUtil::dirToAngle(lookDir) + 180;
	setRotation(rotation);
}

void Ship::fire()
{
	projectileSpawner.spawnProjectile(
		getPosition(),
		getRotation(),
		projectileDuration,
		"PlayerProjectile");
}

void Ship::reset()
{
	setPosition((float)Application::getScrnW() / 2, (float)Application::getScrnH() / 2);
	setRotation(0.f);
	velocity = sf::Vector2f(0, 0);
	setEnabled(true);
	projectileSpawner.despawnAll();
}

void Ship::handleCollision(Entity& other)
{
	if (other.name == "UFO" || other.name == "Asteroid" || other.name == "EnemyProjectile")
	{
		setEnabled(false);
		destroyed = true;
	}
}

void Ship::despawnProjectiles()
{
	projectileSpawner.despawnAll();
}

sf::FloatRect Ship::getBounds() const
{
	return shipShape.getGlobalBounds();
}

bool Ship::collisionEnabled() const
{
	return isEnabled();
}

bool Ship::isDestroyed() const
{
	return destroyed;
}