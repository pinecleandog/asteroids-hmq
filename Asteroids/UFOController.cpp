#include "UFOController.h"
#include "GameManager.h"
#include "MathUtil.h"
#include "Application.h"
#include "Ship.h"

void UFOController::begin()
{
	auto activeScene = GameManager::GetInstance().getActiveScene();
	if (activeScene == nullptr) return;

	if (ufo == nullptr)
	{
		// Create UFO is not already created
		ufo = activeScene->spawnEntity<UFO>();
		ufo->onUFODestroyed = std::bind(&UFOController::onUFODestroyed, this, std::placeholders::_1);
	}

	resetSpawnTimer();

	enabled = true;
}

void UFOController::update(float deltaTime)
{
	if (!enabled) return;

	timeToSpawn -= deltaTime;

	if (timeToSpawn <= 0)
	{
		resetSpawnTimer();

		// Respawn UFO if it's not enabled and the reset timer has struck
		if (!ufo->isEnabled())
		{
			float spawnX = MathUtil::random(0, float(Application::getScrnW()));
			auto activeScene = GameManager::GetInstance().getActiveScene();

			if (activeScene == nullptr) return;

			auto ship = activeScene->findEntity<Entities::Ship>("Ship");
			if (ship == nullptr) return;

			ufo->spawn(sf::Vector2f(spawnX, spawnY), ship);
		}
	}
}

void UFOController::advanceLevel(int level)
{
	ufo->despawnProjectiles();
	ufo->setEnabled(false);
	resetSpawnTimer();
}

void UFOController::end()
{
	ufo->despawnProjectiles();
	ufo->setEnabled(false);
	enabled = false;
}

void UFOController::onUFODestroyed(UFO& ufo)
{
	GameManager::GetInstance().ufoDestroyed();
	resetSpawnTimer();
}

void UFOController::resetSpawnTimer()
{
	timeToSpawn = MathUtil::random(minSpawnDelay, maxSpawnDelay);
}