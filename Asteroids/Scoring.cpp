#include "Scoring.h"

Scoring::Scoring() : score(ObservableInt("Score")) { }


void Scoring::asteroidDestroyed(AsteroidLevel level)
{
	int val = 0;
	switch (level)
	{
	case AsteroidLevel::LARGE:
		val = largeAsteroidValue;
		break;
	case AsteroidLevel::MEDIUM:
		val = mediumAsteroidValue;
		break;
	case AsteroidLevel::SMALL:
		val = smallAsteroidValue;
		break;
	}
	
	score.set(score.get() + val);
}

void Scoring::ufoDestroyed()
{
	score.set(score.get() + ufoValue);
}

void Scoring::reset()
{
	score.set(0);
}