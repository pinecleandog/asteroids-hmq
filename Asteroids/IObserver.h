#pragma once

#include "NotifyEvent.h"

// Implementable by a system that needs to observer an ObservableObject or more.
// This allows the class to react to changes in the Observable's value
class IObserver
{
public:
	virtual void onSubjectChanged(const NotifyEvent& ev) = 0;
};