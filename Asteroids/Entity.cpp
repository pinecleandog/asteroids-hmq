#include "Entity.h"

Entity::Entity()
{
	name = "Entity";
}

void Entity::update(float deltaTime)
{

}

void Entity::setEnabled(bool enabled)
{
	this->enabled = enabled;
}

bool Entity::isEnabled() const
{
	return enabled;
}