#include "PlayerController.h"
#include "GameManager.h"
#include <SFML\Graphics.hpp>
#include "Application.h"
#include "Projectile.h"

using namespace Entities;

void PlayerController::begin()
{
	auto activeScene = GameManager::GetInstance().getActiveScene();

	if (activeScene == nullptr) return;

	ship = activeScene->spawnEntity<Ship>();
	ship->reset();
}

void PlayerController::update(float deltaTime)
{
	if (ship->isDestroyed())
	{
		GameManager::GetInstance().end();
	}

	processInput(deltaTime);
}

void PlayerController::processInput(float deltaTime)
{
	// Handle movement input and send to ship
	sf::Vector2i moveInput;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W))
		moveInput.y = -1;
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S))
		moveInput.y = 1;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D))
		moveInput.x = 1;
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A))
		moveInput.x = -1;

	ship->setMovement(moveInput);

	// Handle mouse input and set ships aim direction

	sf::Vector2i mousePos = sf::Mouse::getPosition(*Application::getWindow());
	auto lookPos = sf::Vector2f(Application::getWindow()->mapPixelToCoords(
		mousePos, Application::getWindow()->getView()));

	ship->setLookPos(lookPos);

	// Handle shooting input

	if (timeSinceLastFire > fireDelay)
	{
		if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && !firedThisClick)
		{
			ship->fire();

			timeSinceLastFire = 0;
			firedThisClick = true;
		}
		else if (firedThisClick && !sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
		{
			firedThisClick = false;
		}
	}

	timeSinceLastFire += deltaTime;
}


void PlayerController::advanceLevel(int level)
{
	ship->reset();
}

void PlayerController::end()
{
	ship->despawnProjectiles();
}