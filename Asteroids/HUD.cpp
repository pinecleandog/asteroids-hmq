#include "HUD.h"
#include <SFML\Graphics\Font.hpp>
#include "Application.h"

HUD::HUD()
{
	// Init scoreText
	scoreText.setCharacterSize(24);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(10.f, 0.f);
	// Init startGameText
	startGameText.setString("Press Space to play!");
	startGameText.setCharacterSize(38);
	startGameText.setFillColor(sf::Color::White);
}

void HUD::setFont(std::string fontName)
{
	if (!font.loadFromFile(fontName)) return;

	scoreText.setFont(font);
	startGameText.setFont(font);
}

void HUD::setScore(int score)
{
	scoreText.setString(std::to_string(score));
}

void HUD::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
{
	rt.draw(scoreText, rs);

	if(displayStartGame)
		rt.draw(startGameText, rs);
}

void HUD::onSubjectChanged(const NotifyEvent& ev)
{
	if (ev.name == "Score")
	{
		setScore(ev.data.iVal);
	}
}

void HUD::startGameTextVisible(bool visible)
{
	displayStartGame = visible;

	// recalculate position
	float xPos = Application::getScrnW() / 2.f - startGameText.getLocalBounds().width / 2.f;
	float yPos = Application::getScrnH() / 2.f - startGameText.getLocalBounds().height / 2.f;
	startGameText.setPosition(xPos, yPos);
}