#pragma once

#include <memory>
#include "UFO.h"
#include "IGameplayController.h"

// Controls the UFO spawning and inputs.
class UFOController : public IGameplayController
{
public:

	// IGameplayController Implementation

	void begin() override;
	void update(float deltaTime) override;
	void advanceLevel(int level) override;
	void end() override;

private:

	void onUFODestroyed(UFO& ufo);
	void resetSpawnTimer();

	// Minimum time it takes to spawn
	const float minSpawnDelay = 5.f;
	// Maximum time it takes to spawn
	const float maxSpawnDelay = 15.f;
	// The Y-coordinate on the screen that the ship will spawn at. It will
	// always spawn at a random X coordinate
	const float spawnY = -10.f;

	std::shared_ptr<UFO> ufo;
	float timeToSpawn = 0.f;
	bool enabled;
};