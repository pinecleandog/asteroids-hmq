#pragma once
#include "Observable.h"
#include "Asteroid.h"

// Controls the scoring logic of the game
class Scoring
{
public:
	Scoring();

	// Called when an asteroid is destroyed
	void asteroidDestroyed(AsteroidLevel level);
	// Called when a UFO is destroyed
	void ufoDestroyed();
	// Reset the score
	void reset();

	const int largeAsteroidValue = 20;
	const int mediumAsteroidValue = 50;
	const int smallAsteroidValue = 100;
	const int ufoValue = 200;

	ObservableInt score;
};