
#include "Application.h"

int main()
{
	Application application;

	while (application.isRunning())
	{
		application.update();
	}
	
	return 0;
}