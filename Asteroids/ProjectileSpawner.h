#pragma once

#include <vector>
#include <memory>
#include <string>
#include "Pool.h"
#include "Projectile.h"

// Creates, spawns and manages Projectile objects. Used by both the player and UFO
class ProjectileSpawner
{
public:
	// Spawns a projectile at position <pos> facing <angle> that lasts for <duration> with the name of <name>
	// Name is useful for identifying who the projectile belongs to 
	void spawnProjectile(sf::Vector2f pos, float angle, float duration, std::string name);
	// Update the spawner
	void update(float deltaTime);
	// Despawn any currently spawned projectile owned by this spawner
	void despawnAll();
private:
	void reclaimProjectiles();

	std::vector<std::shared_ptr<Projectile>> activeProjectiles;
	Pool<std::shared_ptr<Projectile>> pooledProjectiles;
};

