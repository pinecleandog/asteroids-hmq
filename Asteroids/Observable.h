#pragma once

#include "IObserver.h"
#include <vector>
#include <string>

// Wraps a type so that when the value of the type is changed, an observer
// is notified what has changed and what the new value is. This is an abstract
// class that needs to have implementations created per object type
template<typename T>
class Observable
{
public:

	Observable(std::string name)
	{
		this->name = name;
	}

	// Adds an IObserver to the list of observers for the object
	void addObserver(IObserver& observer)
	{
		observers.push_back(&observer);
	}
	// Removes an observer for the object
	void removeObserver(IObserver& observer)
	{
		auto it = std::find(observers.begin(), observers.end(), &observer);
		if (it != observers.end())
		{
			observers.erase(it);
		}
	}
	// Sets the internal stored value and notifies any observer
	virtual void set(T val) = 0;
	// Gets the internally stored value
	T get() const
	{
		return value;
	}

protected:
	T value;
	std::string name;
	std::vector<IObserver*> observers;
};

// Specific data type implementations

// Observable implementation for integers
class ObservableInt : public Observable<int>
{
public:
	ObservableInt(std::string name) : Observable<int>(name) {}

	void set(int val) override
	{
		value = val;

		NotifyEvent ev;
		ev.name = name;
		ev.data.iVal = value;

		for (auto observer : observers)
		{
			observer->onSubjectChanged(ev);
		}
	}
};

// Observable implementation for floats
class ObservableFloat : public Observable<float>
{
public:
	ObservableFloat(std::string name) : Observable<float>(name) {}

	void set(float val) override
	{
		value = val;

		NotifyEvent ev;
		ev.name = name;
		ev.data.fVal = value;

		for (auto observer : observers)
		{
			observer->onSubjectChanged(ev);
		}
	}
};
