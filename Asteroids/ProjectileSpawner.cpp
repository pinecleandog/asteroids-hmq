#include "ProjectileSpawner.h"
#include "GameManager.h"

void ProjectileSpawner::update(float deltaTime)
{
	reclaimProjectiles();
}

void ProjectileSpawner::spawnProjectile(sf::Vector2f pos, float angle, float duration, std::string name)
{
	auto activeScene = GameManager::GetInstance().getActiveScene();

	if (activeScene != nullptr)
	{
		std::shared_ptr<Projectile> proj = pooledProjectiles.fetch([&]()
		{
			return activeScene->spawnEntity<Projectile>();
		});

		proj->spawn(pos, angle, duration);
		proj->name = name;
		activeProjectiles.push_back(proj);
	}
}

void ProjectileSpawner::reclaimProjectiles()
{
	// Find any projectiles in the active list that are disabled and reclaim them
	auto it = activeProjectiles.begin();
	while (it != activeProjectiles.end())
	{
		if (*it != nullptr && !(*it)->isEnabled())
		{
			pooledProjectiles.free(*it);
			it = activeProjectiles.erase(it);
			continue;
		}

		it++;
	}
}

void ProjectileSpawner::despawnAll()
{
	for (auto projectile : activeProjectiles)
	{
		projectile->setEnabled(false);
		pooledProjectiles.free(projectile);
	}

	activeProjectiles.clear();
}