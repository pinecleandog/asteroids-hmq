#pragma once

#include "Entity.h"
#include "Scene.h"
#include <vector>
#include "IGameplayController.h"
#include <memory>
#include "HUD.h"
#include "Scoring.h"
#include "Asteroid.h"

// Main class that control's the game's state, and drives the core gameplay systems
// based on the state.
class GameManager
{
public:
	GameManager();
	// Gets the instance of the singleton
	static GameManager& GetInstance();
	// Initialises the game manager
	void init();
	// Starts a new game
	void begin();
	// Called every frame - updates all gameplay systems
	void update(float deltaTime);
	// Starts the next level of the game
	void advanceLevel();
	// Ends the current game
	void end();
	// Gets a reference to the game's HUD class
	HUD& getHUD();
	// Gets a reference to the scene, the structure that contains all the entities
	Scene* getActiveScene();
	
	// TODO: These needs to be handled better and not part of the GameManager --
	
	void asteroidDestroyed(AsteroidLevel level);
	void ufoDestroyed();

private:
	static std::unique_ptr<GameManager> instance;
	bool running = false;
	Scene scene;
	HUD hud;
	Scoring scoring;
	std::vector<std::unique_ptr<IGameplayController>> controllers;
	int level = 0;
};