#include "GameManager.h"
#include "Ship.h"
#include <SFML/Graphics.hpp>
#include "Asteroid.h"
#include "MathUtil.h"
#include "PlayerController.h"
#include "UFOController.h"
#include "AsteroidController.h"
#include <memory>

using namespace Entities;

std::unique_ptr<GameManager> GameManager::instance = nullptr;

GameManager::GameManager()
{
	controllers.push_back(std::make_unique<PlayerController>());
	controllers.push_back(std::make_unique<UFOController>());
	controllers.push_back(std::make_unique<AsteroidController>());

	hud.setFont("sansation.ttf");
	scoring.score.addObserver(hud);
	scoring.reset();
}

void GameManager::init()
{
	running = false;
	hud.startGameTextVisible(true);
}

void GameManager::begin()
{
	level = 0;
	scoring.score.set(0);
	hud.startGameTextVisible(false);

	for (auto it = controllers.begin(); it != controllers.end(); it++)
	{
		(*it)->begin();
	}
	
	running = true;
}

void GameManager::update(float deltaTime)
{
	if (running)
	{
		for (auto it = controllers.begin(); it != controllers.end(); it++)
		{
			(*it)->update(deltaTime);
		}
	}
	else
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			begin();
		}
	}

	scene.update(deltaTime);
}

void GameManager::advanceLevel()
{
	level++;

	for (auto it = controllers.begin(); it != controllers.end(); it++)
	{
		(*it)->advanceLevel(level);
	}
}

void GameManager::end()
{
	for (auto it = controllers.begin(); it != controllers.end(); it++)
	{
		(*it)->end();
	}

	running = false;
	hud.startGameTextVisible(true);
}

Scene* GameManager::getActiveScene()
{
	return &scene;
}

// not auto initialised
GameManager& GameManager::GetInstance()
{
	if (instance == nullptr)
	{
		instance = std::make_unique<GameManager>();
	}

	return *instance;
}

HUD& GameManager::getHUD()
{
	return hud;
}

void GameManager::asteroidDestroyed(AsteroidLevel level)
{
	scoring.asteroidDestroyed(level);
}

void GameManager::ufoDestroyed()
{
	scoring.ufoDestroyed();
}