#pragma once

#include "Entity.h"
#include <SFML\Graphics\ConvexShape.hpp>
#include "ICollidable.h"
#include "ProjectileSpawner.h"

namespace Entities
{
	// The player ship that can be sent movement input to move like a twin stick
	// shooter. The ship wil always face the look position that has been set
	class Ship : public Entity, public ICollidable
	{
	public:
		Ship();
		// Resets the ship to the centre of the screen
		void reset();
		// Sets the movement input for the ship
		void setMovement(sf::Vector2i input);
		// Set the position for the ship to face
		void setLookPos(sf::Vector2f pos);
		// Is the ship currently destroyed
		bool isDestroyed() const;
		// Fire a projectile
		void fire();
		// Makes all the projectiles fired from the UFO despawn
		void despawnProjectiles();

		// Entity Implementation

		void update(float deltaTime) override;
		void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override;

		// ICollidable Implementation

		void handleCollision(Entity& other) override;
		sf::FloatRect getBounds() const override;
		bool collisionEnabled() const override;

	private:
		void buildShape();
		void applyMovement(float deltaTime);
		void applyRotation(float deltaTime);

		const float maxSpeed = 100;
		const float acceleration = 75;
		const float projectileDuration = 3.f;

		sf::ConvexShape shipShape;
		sf::Vector2i moveInput;
		sf::Vector2f lookPos;
		sf::Vector2f velocity;
		bool destroyed = false;
		ProjectileSpawner projectileSpawner;
	};
}
