#pragma once

#include "Entity.h"
#include <SFML\Graphics.hpp>

// Anything that implements ICollidable will be tested when checking overlaps.
// Current implementation is simple and only allows for bounding box based collisions
class ICollidable
{
public:
	// If the object has collided with another, this will be called
	// so the collision can be handled
	virtual void handleCollision(Entity& other) =0;
	// allows user to specify collision bounds from any shape, in local space
	virtual sf::FloatRect getBounds() const =0;	
	// specifies if the collision is enabled for this object or not.
	virtual bool collisionEnabled() const = 0;
};