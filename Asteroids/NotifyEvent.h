#pragma once

#include <string>

// Data used in the NotifyEvent object
union NotifyEventData
{
	float fVal;
	double dVal;
	int iVal;
	char* sVal;
	char cVal;
	unsigned int uVal;
};

// A NotifyEvent object is generated when an Observable's value is changed
// It provides the name of the value, and the value so that an observer can react
struct NotifyEvent
{
	std::string name;
	NotifyEventData data;
};