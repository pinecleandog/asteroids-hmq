#include "Asteroid.h"
#include "MathUtil.h"
#include "Application.h"

Asteroid::Asteroid()
{
	setEnabled(false);
	name = "Asteroid";
}

void Asteroid::spawn(AsteroidLevel level, sf::Vector2f pos, sf::Vector2f dir)
{
	this->level = level;
	setPosition(pos);
	moveDir = dir;
	buildShape();
	speed = ((int)level == 0) ? baseSpeed : baseSpeed + ((int)level * speedMultiplier * baseSpeed);
	setEnabled(true);
}

AsteroidLevel Asteroid::getLevel() const
{
	return level;
}

void Asteroid::update(float deltaTime)
{
	if (!isEnabled()) return;

	sf::Vector2f newPos = getPosition() + moveDir * speed * deltaTime;
	newPos = MathUtil::screenWrapPosition(newPos);
	setPosition(newPos);
}

void Asteroid::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
{
	if (!isEnabled()) return;

	rs.transform *= getTransform();
	rt.draw(asteroidShape, rs);
}

void Asteroid::buildShape()
{
	// create a "template" circle for making the asteroid
	sf::CircleShape circle;
	int points = 12;
	circle.setPointCount(points);
	float size = ((int)level == 0) ? baseSize : baseSize - (sizeMultiplier * (int)level * baseSize);
	circle.setRadius(size);
	// adjust variance based on size
	float maxVariance = 6.f * (size / baseSize);

	// Mess up the circle
	// This doesn't actually make a convex shape
	asteroidShape.setPointCount(points);
	for (int i = 0; i < points; i++)
	{
		auto point = circle.getPoint(i);
		point.x += MathUtil::random(-maxVariance, maxVariance);
		point.y += MathUtil::random(-maxVariance, maxVariance);
		asteroidShape.setPoint(i, point);
	}

	asteroidShape.setFillColor(sf::Color::Black);
	asteroidShape.setOutlineColor(sf::Color::White);
	asteroidShape.setOutlineThickness(1);
	asteroidShape.setPosition(sf::Vector2f(0, 0));
}
void Asteroid::handleCollision(Entity& other)
{
	if (other.name == "PlayerProjectile")
	{
		setEnabled(false);
		if(onAsteroidDestroyed)
			onAsteroidDestroyed(*this);
	}
}

sf::FloatRect Asteroid::getBounds() const
{
	return asteroidShape.getGlobalBounds();
}

bool Asteroid::collisionEnabled() const
{
	return isEnabled();
}