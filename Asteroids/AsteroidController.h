#pragma once

#include "IGameplayController.h"
#include "Pool.h"
#include <memory>
#include "Asteroid.h"
#include <vector>

// Asteroid system - controlls entities and logic.
class AsteroidController : public IGameplayController
{
public:

	// IGameplayController Implementation

	void begin();
	void advanceLevel(int level);
	void update(float deltaTime);
	void end();

private:

	void asteroidDestroyed(Asteroid& asteroid);
	void despawnAll();
	void spawnAsteroidField(int numAsteroids);
	void spawnAsteroid(AsteroidLevel level, sf::Vector2f pos, sf::Vector2f dir);
	int getRemaining() const;

	// Number of asteroids to spawn as a base (and the total for the first level)
	const int baseNumAsteroids = 5;
	// How many extra asteroids are spawned each level
	const int extraAsteroidsPerLevel = 3;
	// Minimum distances asteroids can spawn from the center, to prevent starting on player
	const float minDistFromCenter = 150;
	
	bool running = false;
	Pool<std::shared_ptr<Asteroid>> asteroidPool;
	std::vector<std::shared_ptr<Asteroid>> activeAsteroids;
};