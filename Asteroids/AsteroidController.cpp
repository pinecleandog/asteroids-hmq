#include "AsteroidController.h"
#include "GameManager.h"
#include "Application.h"
#include "MathUtil.h"
#include <functional>

void AsteroidController::begin()
{
	spawnAsteroidField(baseNumAsteroids);
	running = true;
}

void AsteroidController::advanceLevel(int level)
{
	despawnAll();
	spawnAsteroidField(baseNumAsteroids + (extraAsteroidsPerLevel * level));
	running = true;
}

void AsteroidController::update(float deltaTime)
{
	if (!running) return;

	if (getRemaining() < 1)
	{
		GameManager::GetInstance().advanceLevel();
	}
}

void AsteroidController::end()
{
	running = false;
	despawnAll();
}

int AsteroidController::getRemaining() const
{
	return (int)activeAsteroids.size();
}

void AsteroidController::asteroidDestroyed(Asteroid& asteroid)
{
	GameManager::GetInstance().asteroidDestroyed(asteroid.getLevel());

	// Remove asteroid from active astroid list
	std::shared_ptr<Asteroid> oldAsteroid;
	auto it = activeAsteroids.begin();
	while (it != activeAsteroids.end())
	{
		if (it->get() == &asteroid)
		{
			oldAsteroid = *it;
			activeAsteroids.erase(it);
			break;
		}
		it++;
	}

	// Do not spawn smaller ones if the asteroid is a small asteroid
	if (asteroid.getLevel() == AsteroidLevel::SMALL) return;

	int level = (int)asteroid.getLevel() + 1;

	auto activeScene = GameManager::GetInstance().getActiveScene();
	if (activeScene == nullptr) return;

	// Create two new asteroids the next level down
	for (int i = 0; i < 2; i++)
	{
		auto dir = MathUtil::angleToDir(MathUtil::random(0, 360));

		spawnAsteroid((AsteroidLevel)level, asteroid.getPosition(), dir);
	}

	if (oldAsteroid != nullptr)
	{
		asteroidPool.free(oldAsteroid);
	}

	// Unbind delegate
	asteroid.onAsteroidDestroyed = nullptr;
}

void AsteroidController::despawnAll()
{
	for (auto asteroid : activeAsteroids)
	{
		asteroid->setEnabled(false);
		asteroidPool.free(asteroid);
	}

	activeAsteroids.clear();
}

void AsteroidController::spawnAsteroidField(int numAsteroids)
{
	for (int i = 0; i < numAsteroids; i++)
	{
		// Select a random position within screen
		float xPos = MathUtil::random(0, float(Application::getScrnW()));
		float yPos = MathUtil::random(0, float(Application::getScrnH()));
		auto pos = sf::Vector2f(xPos, yPos);

		// If the spawn pos is within min distance from center, move outside of the safe range
		auto centerPos = sf::Vector2f(float(Application::getScrnW()) / 2, float(Application::getScrnH()) / 2);
		float distFromCenter = abs(MathUtil::length(pos - centerPos));

		if (distFromCenter < minDistFromCenter)
		{
			pos = centerPos + MathUtil::normalize(pos - centerPos) * minDistFromCenter;
		}

		auto dir = MathUtil::angleToDir(MathUtil::random(0, 360));

		spawnAsteroid(AsteroidLevel::LARGE, pos, dir);
	}
}

void AsteroidController::spawnAsteroid(AsteroidLevel level, sf::Vector2f pos, sf::Vector2f dir)
{
	auto activeScene = GameManager::GetInstance().getActiveScene();
	if (activeScene == nullptr) return;

	auto asteroid = asteroidPool.fetch([&]()
	{
		return activeScene->spawnEntity<Asteroid>();
	});

	asteroid->spawn(level, pos, dir);
	asteroid->onAsteroidDestroyed =
		std::bind(&AsteroidController::asteroidDestroyed, this, std::placeholders::_1);
	activeAsteroids.push_back(asteroid);
}