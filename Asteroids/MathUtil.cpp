#include "MathUtil.h"
#include <random>
#include <cassert>
#include "Application.h"

sf::Vector2f MathUtil::angleToDir(float angle)
{
	return sf::Vector2f(cos(angle * degToRad), sin(angle * degToRad));
}

float MathUtil::dirToAngle(sf::Vector2f vec)
{
	return atan2(vec.y, vec.x) * radToDeg;
}

sf::Vector2f MathUtil::normalize(sf::Vector2f vec)
{
	float len = length(vec);

	if (len != 0)
	{
		vec.x = vec.x / len;
		vec.y = vec.y / len;
	}

	return vec;
}

float MathUtil::length(sf::Vector2f vec)
{
	return sqrt(vec.x * vec.x + vec.y * vec.y);
}

float MathUtil::clamp(float val, float min, float max)
{
	return std::max(min, std::min(val, max));
}

// solution from stack exchange
// https://stackoverflow.com/questions/1903954/is-there-a-standard-sign-function-signum-sgn-in-c-c
int MathUtil::sign(float val)
{
	return (val > 0) - (val < 0);
}

sf::Vector2f MathUtil::screenWrapPosition(sf::Vector2f pos)
{
	float scrnW = float(Application::getScrnW());
	float scrnH = float(Application::getScrnH());

	if (pos.x < 0.f)
		pos.x = scrnW - (std::abs(pos.x));
	else if (pos.x >= scrnW)
		pos.x = (pos.x - scrnW);

	if (pos.y < 0.f)
		pos.y = scrnH - (std::abs(pos.y));
	else if (pos.y >= scrnH)
		pos.y = (pos.y - scrnH);

	return pos;
}

// Code is from example given in cppreference.com
// http://en.cppreference.com/w/cpp/numeric/random/uniform_real_distribution
float MathUtil::random(float min, float max)
{
	assert(min < max);
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_real_distribution<float> dis(min, max);
	return dis(gen);
}