#pragma once

#include "Entity.h"
#include <SFML\Graphics.hpp>
#include "ICollidable.h"
#include <functional>
#include <memory>

// Level/State of the asteroid as it breaks down
enum class AsteroidLevel
{
	LARGE = 0,
	MEDIUM,
	SMALL
};

// An asteroid that is spawned and moves at a constant speed. Can be shot and
// broken into smaller asteroids.
class Asteroid : public Entity, public ICollidable
{
	typedef std::function<void(Asteroid&)> AsteroidEventDelegate;

public:
	Asteroid();
	// Sets the asteroid to enabled, and spawns it at a position as a <level> asteroid
	void spawn(AsteroidLevel level, sf::Vector2f pos, sf::Vector2f dir);
	// Gets the current level of the asteroid
	AsteroidLevel getLevel() const;
	
	// Entity Implementation

	void update(float deltaTime) override;
	void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override;

	// ICollidable Implementation

	void handleCollision(Entity& other) override;
	sf::FloatRect getBounds()const override;
	bool collisionEnabled()const override;

	// Triggered when the asteroid has been hit by a player projectile
	AsteroidEventDelegate onAsteroidDestroyed;
private:
	void buildShape();

	// how much the speed is increased each size
	const float speedMultiplier = 0.25f;
	// how much the size is decreased each size
	const float sizeMultiplier = 0.33f;
	// Base speed of asteroid in pixels/s (large)
	const float baseSpeed = 25;
	// Base asteroid size in pixels (large)
	const float baseSize = 30;

	AsteroidLevel level;
	sf::Vector2f moveDir;
	sf::ConvexShape asteroidShape;
	float speed;
};
