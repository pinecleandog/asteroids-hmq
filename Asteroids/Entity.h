#pragma once

#include <SFML\Graphics\Transformable.hpp>
#include <SFML\Graphics\Drawable.hpp>
#include <string>

// Base class for anything that exists in a scene and that can be transformed and drawn
class Entity : public sf::Drawable, public sf::Transformable
{
public:
	Entity();
	// Overriden to drive behaviour of the entity
	virtual void update(float deltaTime);
	// Is the entity enabled
	bool isEnabled() const;
	// Set the entity's enabled state
	void setEnabled(bool enabled);

	// The name of the entity.
	std::string name;
private:
	bool enabled = false;
};