#pragma once

#include <vector>
#include <SFML\Graphics\RenderWindow.hpp>
#include "Entity.h"
#include <memory>

// A representation of the game's scene - a collection of entities. Performs
// and update on all entity behaviours and physics, draws entities and provides
// a way to create and find entities in the scene.
class Scene
{
public:

	~Scene();

	// Spawn an entity of type T and returns a pointer to it.
	template<typename T>
	std::shared_ptr<T> spawnEntity()
	{
		auto entity = std::make_shared<T>();
		addEntity(std::shared_ptr<Entity>(entity));
		return std::shared_ptr<T>(entity);
	}

	// Finds the first occurance of an entity with the name <name>. Only useful
	// if the name of the entity is unique
	template<typename T>
	std::shared_ptr<T> findEntity(std::string name) const
	{
		// ideally the vector should be sorted and not require a full linear search
		for (auto it = entities.begin(); it != entities.end(); it++)
		{
			if ((*it)->name == name)
			{
				return std::shared_ptr<T>(std::dynamic_pointer_cast<T>(*it));
			}
		}

		return nullptr;
	}
	// Draw all objects in the scene
	void draw(sf::RenderWindow& window) const;
	// Update all behaviours of entities in the scene
	void update(float deltaTime);

private:
	void addEntity(std::shared_ptr<Entity>& entity);
	sf::FloatRect convertBoundsToGlobal(sf::FloatRect bounds, const Entity& ent);
	std::vector<std::shared_ptr<Entity>> entities;
	std::vector<std::shared_ptr<Entity>> newEntities;
};