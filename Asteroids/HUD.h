#pragma once

#include <SFML\Graphics\Text.hpp>
#include <SFML\Graphics.hpp>
#include <string>
#include "IObserver.h"

// Main HUD class of the game. Drawn after everything else and is used to draw
// the user interface.
class HUD : public sf::Drawable, public IObserver
{
public:
	HUD();
	// Sets the font for all text objects (currently limited to just use one front)
	// TODO: this could be improved
	void setFont(std::string fontName);
	// Draws the HUD elements
	void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override;
	// Sets whether or not the start game text is rendered
	void startGameTextVisible(bool visible);

	// IObserver Implementation

	void onSubjectChanged(const NotifyEvent& ev) override;
private:
	void setScore(int score);

	bool displayStartGame = false;
	sf::Font font;
	sf::Text scoreText;
	sf::Text startGameText;
	bool fontValid;
};