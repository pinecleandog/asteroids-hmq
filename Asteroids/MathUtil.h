#pragma once

#include <SFML\Graphics.hpp>

// Provides math utility methods that are useful anywhere
namespace MathUtil
{
	// Constants

	const float pi = 3.141592f;
	const float degToRad = pi / 180;
	const float radToDeg = 180 / pi;

	// Vectors

	// Converts and angle (in degrees) to a vector (direction)
	sf::Vector2f angleToDir(float angle);
	// Converts a direction (vector) into an angle (degrees)
	float dirToAngle(sf::Vector2f vec);
	// Normalizes a vector to have a length of 1
	sf::Vector2f normalize(sf::Vector2f vec);
	// Returns the length of a vector
	float length(sf::Vector2f vec);

	// Random

	// Returns a uniformly distributed random value between min (inclusive) and
	// max (inclusive)
	float random(float min, float max); 

	// Other

	// Clamps val betwen min and max
	float clamp(float val, float min, float max);
	// Screenwraps a given vector based on the window's width and height.
	sf::Vector2f screenWrapPosition(sf::Vector2f pos);
	// Signs a float
	int sign(float val);
}