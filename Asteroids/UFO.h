#pragma once

#include "Entity.h"
#include <SFML\Graphics\ConvexShape.hpp>
#include "Ship.h"
#include <memory>
#include "ProjectileSpawner.h"
#include "ICollidable.h"

// A UFO is an entity that will spawn from the top of the screen and move
// towards the player, shooting at the player. It can be shot and destroyed
class UFO : public Entity, public ICollidable
{
	typedef std::function<void(UFO&)> UFOEventDelegate;

public:
	UFO();
	// Spawn the ufo at position <pos>, set the target to an instance of the player ship
	void spawn(sf::Vector2f pos, std::shared_ptr<const Entities::Ship> target);
	// Makes all the projectiles fired from the UFO despawn
	void despawnProjectiles();

	// Entity Implementation

	virtual void update(float deltaTime) override;
	void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override;


	// ICollidable Implementation

	void handleCollision(Entity& other) override;
	sf::FloatRect getBounds() const override;
	bool collisionEnabled() const override;

	// Triggered when the UFO has been shot and destroyed
	UFOEventDelegate onUFODestroyed;

protected:
	virtual void buildShape();

	// Speed of the UFO in pixels per second
	const float speed = 45;
	// Delay (in seconds) between shots
	const float shootInterval = 0.7f;
	// How long projectiles (in seconds) should be active
	const float projectileDuration = 2.f;

	float timeToShoot = 0.f;
	sf::ConvexShape shipShape;
	std::shared_ptr<const Entities::Ship> target;
	ProjectileSpawner projectileSpawner;
};