#include "UFO.h"
#include <SFML\Graphics.hpp>
#include "MathUtil.h"

UFO::UFO()
{
	buildShape();
	name = "UFO";
}

void UFO::spawn(sf::Vector2f pos, std::shared_ptr<const Entities::Ship> target)
{
	setEnabled(true);
	this->target = target;
	this->setPosition(pos);
	timeToShoot = shootInterval;
}

void UFO::update(float deltaTime)
{
	if (!isEnabled()) return;

	// Movement
	sf::Vector2f moveDir = target->getPosition() - getPosition();
	moveDir = MathUtil::normalize(moveDir);

	auto newPos = getPosition() + moveDir * speed * deltaTime;
	setPosition(newPos);

	// Shooting
	timeToShoot -= deltaTime;
	if (timeToShoot <= 0)
	{
		timeToShoot = shootInterval;
		projectileSpawner.spawnProjectile(
			getPosition(),
			MathUtil::dirToAngle(moveDir),
			projectileDuration,
			"EnemyProjectile");
	}

	projectileSpawner.update(deltaTime);
}

void UFO::draw(sf::RenderTarget& rt, sf::RenderStates rs) const
{
	if (!isEnabled()) return;

	rs.transform *= getTransform();
	rt.draw(shipShape, rs);
}

void UFO::buildShape() 
{ 
	shipShape.setPointCount(5);
	shipShape.setPoint(0, sf::Vector2f(0.f, -10.f));
	shipShape.setPoint(1, sf::Vector2f(-10.f, -4.f));
	shipShape.setPoint(2, sf::Vector2f(-15.f, 5.f));
	shipShape.setPoint(3, sf::Vector2f(15.f, 5.f));
	shipShape.setPoint(4, sf::Vector2f(10.f, -4.f));

	shipShape.setFillColor(sf::Color::Black);
	shipShape.setOutlineColor(sf::Color::White);
	shipShape.setOutlineThickness(1.f);

	shipShape.setPosition(0.f, 0.f);
	shipShape.setRotation(0.f);
}

void UFO::handleCollision(Entity& other)
{
	if (other.name == "PlayerProjectile")
	{
		setEnabled(false);
		if (onUFODestroyed)
			onUFODestroyed(*this);
	}
}

sf::FloatRect UFO::getBounds() const
{
	return shipShape.getGlobalBounds();
}

bool UFO::collisionEnabled() const
{
	return isEnabled();
}

void UFO::despawnProjectiles()
{
	projectileSpawner.despawnAll();
}