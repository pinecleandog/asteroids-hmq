#include "Application.h"

#define SCRN_W 600
#define SCRN_H 480

Application* Application::instance = nullptr;

Application::Application()
{
	this->window = new sf::RenderWindow(sf::VideoMode(SCRN_W, SCRN_H), "Hmq Programming Test");
	instance = this;
	running = true;
	GameManager::GetInstance().init();
}

bool Application::isRunning() const
{
	return running;
}

unsigned int Application::getScrnW()
{
	return SCRN_W;
}

unsigned int Application::getScrnH()
{
	return SCRN_H;
}

void Application::update()
{
	sf::Event event;

	while (window->pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			exit();
	}
	
	GameManager::GetInstance().update(frameTimer.restart().asSeconds());
	
	window->clear();
	GameManager::GetInstance().getActiveScene()->draw(*window);
	window->draw(GameManager::GetInstance().getHUD());
	window->display();
}

void Application::exit()
{
	GameManager::GetInstance().end();
	window->close();
	running = false;
}

Application::~Application()
{
	delete window;
	instance = nullptr;
}

const sf::RenderWindow* Application::getWindow()
{
	if (instance == nullptr) return nullptr;

	return instance->window;
}