#pragma once

#include <memory>
#include "Ship.h"
#include "IGameplayController.h"

// Controlls the ship and projectiles based on player input.
class PlayerController : public IGameplayController
{
public:

	// IGameplayController Implementation

	void begin();
	void update(float deltaTime);
	void advanceLevel(int level);
	void end();

private:
	void processInput(float deltaTime);

	// Minimum delay required between each shot
	const float fireDelay = 0.1f;

	std::shared_ptr<Entities::Ship> ship;
	bool firedThisClick = false;
	float timeSinceLastFire;
};