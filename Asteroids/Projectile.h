#pragma once

#include "Entity.h"
#include <SFML\Graphics\ConvexShape.hpp>
#include "ICollidable.h"

// A projectile that moves at a constant velocity, used by an enemy or a player
// The projectile can be tagged by setting the name
class Projectile : public Entity, public ICollidable
{
public:
	Projectile();
	// Spawns the projectile and enables it at a <pos>, facing <angle>, and
	// defines how long it should last
	void spawn(sf::Vector2f pos, float angle, float duration);

	// Entity Implementation

	void draw(sf::RenderTarget& rt, sf::RenderStates rs) const override;
	void update(float deltaTime) override;

	// ICollidable Implementation

	void handleCollision(Entity& other) override;
	sf::FloatRect getBounds() const override;
	bool collisionEnabled() const override;

private:
	const float speed = 150.f;
	const float length = 3.f;

	sf::Vector2f direction;
	float timeLeft;
	sf::FloatRect bounds;
};