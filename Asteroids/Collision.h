#pragma once

#include <memory>
#include "ICollidable.h"

// Wraps data generated from a collision, so it may be handled at a later time.
// None of the data can be guarenteed to exist at the time of processing.
struct Collision
{
	Collision(
		int id,
		Entity* ent1,
		Entity* ent2,
		ICollidable* col1,
		ICollidable* col2)
	{
		this->ent1 = ent1;
		this->ent2 = ent2;
		this->col1 = col1;
		this->col2 = col2;
	}

	int id;
	Entity* ent1;
	ICollidable* col1;
	Entity* ent2;
	ICollidable* col2;

	// these need to remain as non member functions

	friend bool operator==(const Collision& lhs, const Collision& rhs)
	{
		return lhs.id == rhs.id;
	}

	friend bool operator!=(const Collision& lhs, const Collision& rhs)
	{
		return !(lhs == rhs);
	}

	friend bool operator< (const Collision& lhs, const Collision& rhs)
	{
		return lhs.id < rhs.id;
	}

	friend bool operator> (const Collision& lhs, const Collision& rhs)
	{
		return lhs.id > rhs.id;
	}
};