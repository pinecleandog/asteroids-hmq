#pragma once

#include <vector>
#include <vector>
#include <functional>

// A simple implementation of a pool that allows re-use of objects, and 
// instantiates new ones if non are available for re-use. Do note that this
// implementation does not reset objects
template<typename T>
class Pool
{
public:

	// Fetches an object instance that has been stored, or create a new one otherwise
	// A lambda must be passed to define how an object should be created, if there 
	// are none available for re-use
	T fetch(std::function<T()> createObject)
	{
		T obj = nullptr;

		if (pool.size() > 0)
		{
			obj = pool.back();
			pool.pop_back();
		}
		else
		{
			obj = createObject();
		}

		return obj;
	}
	// Frees an object back into the pool for re-use
	void free(T obj)
	{
		pool.push_back(obj);
	}

private:
	std::vector<T> pool;
};